//
//  ViewController.h
//  testResto
//
//  Created by Vinay Chopra on 24/07/14.
//  Copyright (c) 2014 Tresbu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MapViewAnnotation.h"

@interface ViewController : UIViewController<MKMapViewDelegate>
@property (nonatomic, strong) IBOutlet MKMapView *mapView;

@end
