//
//  AppDelegate.h
//  testResto
//
//  Created by Vinay Chopra on 24/07/14.
//  Copyright (c) 2014 Tresbu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
