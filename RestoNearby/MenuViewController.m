//
//  MenuViewController.m
//  testResto
//
//  Created by Vinay Chopra on 25/07/14.
//  Copyright (c) 2014 Tresbu. All rights reserved.
//

#import "MenuViewController.h"

@interface MenuViewController ()

@end

@implementation MenuViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    UIAlertView * alert= [[UIAlertView alloc]initWithTitle:@"Split the Bill" message:@"In a real tab you will be able to split the bill with your friends!" delegate:self cancelButtonTitle:@"Awesome!" otherButtonTitles:nil, nil];
    [alert show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    self.tabBarController.selectedViewController
    = [self.tabBarController.viewControllers objectAtIndex:0];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
