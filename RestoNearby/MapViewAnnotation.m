//
//  MapViewAnnotation.m
//  MapViewExample1
//
//  Created by Ravi Shankar on 04/01/14.
//  Copyright (c) 2014 Ravi Shankar. All rights reserved.
//

#import "MapViewAnnotation.h"

@implementation MapViewAnnotation

@synthesize coordinate=_coordinate;
@synthesize title=_title;

-(id) initWithTitle:(NSString *) title AndCoordinate:(CLLocationCoordinate2D)coordinate
{
    self =  [super init];
    _title = title;
    _coordinate = coordinate;
    return self;
}


//Read locations details from plist
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"locations" ofType:@"plist"];
//    NSArray *locations = [NSArray arrayWithContentsOfFile:path];
//
//    for (NSDictionary *row in locations) {
//        NSNumber *latitude = [row objectForKey:@"latitude"];
//        NSNumber *longitude = [row objectForKey:@"longitude"];
//        NSString *title = [row objectForKey:@"title"];
//
//        //Create coordinates from the latitude and longitude values
//        CLLocationCoordinate2D coord;
//        coord.latitude = latitude.doubleValue;
//        coord.longitude = longitude.doubleValue;
//
//        MapViewAnnotation *annotation = [[MapViewAnnotation alloc] initWithTitle:title AndCoordinate:coord];
//        [annotations addObject:annotation];
//    }
@end
