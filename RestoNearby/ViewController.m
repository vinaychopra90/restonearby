//
//  ViewController.m
//  testResto
//
//  Created by Vinay Chopra on 24/07/14.
//  Copyright (c) 2014 Tresbu. All rights reserved.
//

#import "ViewController.h"
#import <MapKit/MapKit.h>
#define METERS_PER_MILE 1609.344


@interface ViewController ()
@property(nonatomic,strong)MKUserLocation * currentUserLocation;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.mapView.delegate = self;

//    [self zoomToLocation];
}

- (NSMutableArray *)createAnnotations
{
    NSMutableArray *annotations = [[NSMutableArray alloc] init];
    
    
    CLLocationCoordinate2D coord;
    coord.latitude = _currentUserLocation.coordinate.latitude +.00123;
    coord.longitude = _currentUserLocation.coordinate.longitude+.00123;
    
    MapViewAnnotation *annotation = [[MapViewAnnotation alloc] initWithTitle:@"Coming Soon" AndCoordinate:coord];
    [annotations addObject:annotation];
    coord.latitude = _currentUserLocation.coordinate.latitude +.00323;
    coord.longitude = _currentUserLocation.coordinate.longitude+.00123;
    
    MapViewAnnotation *annotation2 = [[MapViewAnnotation alloc] initWithTitle:@"Coming Soon" AndCoordinate:coord];
    [annotations addObject:annotation2];
    
    
    coord.latitude = _currentUserLocation.coordinate.latitude -.00023;
    coord.longitude = _currentUserLocation.coordinate.longitude-.00123;
    
    MapViewAnnotation *annotation3 = [[MapViewAnnotation alloc] initWithTitle:@"Coming Soon" AndCoordinate:coord];
    [annotations addObject:annotation3];
    
    coord.latitude = _currentUserLocation.coordinate.latitude +.00323;
    coord.longitude = _currentUserLocation.coordinate.longitude+.0023;
    
    MapViewAnnotation *annotation4 = [[MapViewAnnotation alloc] initWithTitle:@"Coming Soon" AndCoordinate:coord];
    [annotations addObject:annotation4];
    
    
    coord.latitude = _currentUserLocation.coordinate.latitude -.00323;
    coord.longitude = _currentUserLocation.coordinate.longitude-.0023;
    
    MapViewAnnotation *annotation5 = [[MapViewAnnotation alloc] initWithTitle:@"Coming Soon" AndCoordinate:coord];
    [annotations addObject:annotation5];
    
    
    //Read locations details from plist
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"locations" ofType:@"plist"];
//    NSArray *locations = [NSArray arrayWithContentsOfFile:path];
//    
//    for (NSDictionary *row in locations) {
//        NSNumber *latitude = [row objectForKey:@"latitude"];
//        NSNumber *longitude = [row objectForKey:@"longitude"];
//        NSString *title = [row objectForKey:@"title"];
//        
//        //Create coordinates from the latitude and longitude values
//        CLLocationCoordinate2D coord;
//        coord.latitude = latitude.doubleValue;
//        coord.longitude = longitude.doubleValue;
//        
//        MapViewAnnotation *annotation = [[MapViewAnnotation alloc] initWithTitle:title AndCoordinate:coord];
//        [annotations addObject:annotation];
//    }
    
    //Read locations details from plist
    //    NSString *path = [[NSBundle mainBundle] pathForResource:@"locations" ofType:@"plist"];
    //    NSArray *locations = [NSArray arrayWithContentsOfFile:path];
    //
    //    for (NSDictionary *row in locations) {
    //        NSNumber *latitude = [row objectForKey:@"latitude"];
    //        NSNumber *longitude = [row objectForKey:@"longitude"];
    //        NSString *title = [row objectForKey:@"title"];
    //
    //        //Create coordinates from the latitude and longitude values
    //        CLLocationCoordinate2D coord;
    //        coord.latitude = latitude.doubleValue;
    //        coord.longitude = longitude.doubleValue;
    //
    //        MapViewAnnotation *annotation = [[MapViewAnnotation alloc] initWithTitle:title AndCoordinate:coord];
    //        [annotations addObject:annotation];
    //    }
    return annotations;
}

- (void)zoomToLocation
{
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = 13.03297;
    zoomLocation.longitude= 80.26518;
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 7.5*METERS_PER_MILE,7.5*METERS_PER_MILE);
    [self.mapView setRegion:viewRegion animated:YES];
    [self.mapView regionThatFits:viewRegion];
}


- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 1000, 1000);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    
    _currentUserLocation=userLocation;
    // Add an annotation
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = userLocation.coordinate;
    point.title = @"This is my current location";
    point.subtitle = @"I'm here!!!";
    
    // NS log
    
    [self.mapView addAnnotation:point];

    [self.mapView addAnnotations:[self createAnnotations]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
